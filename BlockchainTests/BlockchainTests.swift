//
//  BlockchainTests.swift
//  BlockchainTests
//
//  Created by Nguyễn Thanh Trí on 10/14/18.
//  Copyright © 2018 TriNguyen. All rights reserved.
//

import XCTest
@testable import Blockchain

class BlockchainTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let result = Helper.getNumberTen()
        XCTAssert(result == 10, "Error: Invalid value!!!")
    }
    
}
